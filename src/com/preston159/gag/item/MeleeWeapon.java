package com.preston159.gag.item;

public abstract class MeleeWeapon extends Weapon {
	
	public MeleeWeapon(Items item, double minDamage, double maxDamage) {
		super(item, minDamage, maxDamage);
	}
	
}
