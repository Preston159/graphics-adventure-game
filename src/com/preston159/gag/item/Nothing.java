package com.preston159.gag.item;

import com.preston159.gag.character.Player;

public class Nothing extends Item {
	
	private static final Items ITEM = Items.NOTHING;
	
	public Nothing() {
		super(ITEM, ITEM.toString());
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof Nothing);
	}
	
	public Item copy() {
		return new Nothing();
	}
	
	@Override
	public boolean use(Player p) {
		return false;
	}
	
}
