package com.preston159.gag.world;

public class Tuple<E extends Number> {
	
	public E x, y;
	
	public Tuple(E x, E y) {
		this.x = x;
		this.y = y;
	}
	
	public Tuple<E> copy() {
		return new Tuple<E>(this.x, this.y);
	}
	
}
