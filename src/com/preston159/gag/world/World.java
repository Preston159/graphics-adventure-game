package com.preston159.gag.world;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import com.preston159.gag.display.Settings;
import com.preston159.gag.display.GameObject;
import com.preston159.gag.display.Sprite;
import com.preston159.gag.display.Window;

public class World {
	
	private ObjectList objects = new ObjectList();
	private int w, h;
	private Tuple<Double> offset;
	
	public World(int w, int h, double x, double y) {
		this.w = w;
		this.h = h;
		offset = new Tuple<Double>(x, y);
		drawEdges();
	}
	
	public World(int w, int h) {
		this(w, h, 0, 0);
	}
	
	private void drawEdges() {
		BufferedImage eowImg = null;
		try {
			eowImg = ImageIO.read(new File("img/eow.png"));
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		Sprite eowSprite = new Sprite(eowImg, Settings.SQUARE_SIZE, Settings.SQUARE_SIZE);
		int num = (2 * this.w) + (2 * this.h) + 4;
		ArrayList<Location> locations = new ArrayList<Location>(num);
		locations.add(new Location(-1, -1));
		locations.add(new Location(-1, this.h));
		locations.add(new Location(this.w, -1));
		locations.add(new Location(this.w, this.h));
		for(int x = 0;x < this.w;x++) {
			locations.add(new Location(x, -1));
			locations.add(new Location(x, this.h));
		}
		for(int y = 0;y < this.h;y++) {
			locations.add(new Location(-1, y));
			locations.add(new Location(this.w, y));
		}
		for(int i = 0;i < num;i++) {
			this.addObject(new GameObject(eowSprite, locations.get(i), false));
		}
	}
	
	public int getWidth() {
		return w * Settings.SQUARE_SIZE;
	}
	
	public int getHeight() {
		return h * Settings.SQUARE_SIZE;
	}
	
	public int getSquareWidth() {
		return w;
	}
	
	public int getSquareHeight() {
		return h;
	}
	
	public double getXOff() {
		return offset.x;
	}
	
	public double getYOff() {
		return offset.y;
	}
	
	public void changeXOff(double dx) {
		offset.x += dx;
	}
	
	public void changeYOff(double dy) {
		offset.y += dy;
	}
	
	public Tuple<Double> getOffset() {
		return offset.copy();
	}
	
	public ArrayList<GameObject> getVisibleObjects(Window window) {
		ArrayList<GameObject> visible = this.objects.getList();
		for(int i = visible.size() - 1;i >= 0;i--) {
			if(!visible.get(i).isVisible(window, this.offset)) {
				visible.remove(i);
			}
		}
		return visible;
	}
	
	public void addObject(GameObject object) {
		this.objects.add(object, this.getSquareWidth());
	}
	
	public void sortList() {
		this.objects.sort(this.getSquareWidth());
	}
	
}
