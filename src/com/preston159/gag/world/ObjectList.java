package com.preston159.gag.world;

import java.util.ArrayList;

import com.preston159.gag.display.GameObject;

public class ObjectList {
	
	private boolean sortLock = false;
	
	private ArrayList<GameObject> list = new ArrayList<GameObject>();
	
	public ObjectList() {
		
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<GameObject> getList() {
		return (ArrayList<GameObject>) list.clone();
	}
	
	public void add(GameObject object, int width) {
		if(list.size() == 0) {
			list.add(object);
			return;
		}
		int index = object.getIndex(width);
		boolean added = false;
		for(int i = 0;i < list.size();i++) {
			if(index <= list.get(i).getIndex(width)) {
				list.add(i, object);
				added = true;
				break;
			}
		}
		if(!added) {
			list.add(object);
		}
	}
	
	public GameObject[] getForeground(int x, int y, int width) {
		int index = (y * width) + x;
		ArrayList<GameObject> found = new ArrayList<GameObject>();
		for(int i = 0;i < list.size();i++) {
			if(list.get(i).getIndex(width) < index)
				continue;
			if(list.get(i).getIndex(width) == index) {
				if(list.get(i).isForeground())
					found.add(list.get(i));
				continue;
			}
			break;
		}
		GameObject[] arr = new GameObject[found.size()];
		for(int i = 0;i < arr.length;i++) {
			arr[i] = found.get(i);
		}
		return arr;
	}
	
	public GameObject[] getBackground(int x, int y, int width) {
		int index = (y * width) + x;
		ArrayList<GameObject> found = new ArrayList<GameObject>();
		for(int i = 0;i < list.size();i++) {
			if(list.get(i).getIndex(width) < index)
				continue;
			if(list.get(i).getIndex(width) == index) {
				if(!list.get(i).isForeground())
					found.add(list.get(i));
				continue;
			}
			break;
		}
		GameObject[] arr = new GameObject[found.size()];
		for(int i = 0;i < arr.length;i++) {
			arr[i] = found.get(i);
		}
		return arr;
	}
	
	public GameObject[] getAll(int x, int y, int width) {
		int index = (y * width) + x;
		ArrayList<GameObject> found = new ArrayList<GameObject>();
		for(int i = 0;i < list.size();i++) {
			if(list.get(i).getIndex(width) < index)
				continue;
			if(list.get(i).getIndex(width) == index) {
				found.add(list.get(i));
				continue;
			}
			break;
		}
		GameObject[] arr = new GameObject[found.size()];
		for(int i = 0;i < arr.length;i++) {
			arr[i] = found.get(i);
		}
		return arr;
	}
	
	public void sort(int width) {
		if(sortLock)
			return;
		sortLock = true;
		int n = list.size();
		for(int i = 0;i < n - 1;i++) {
			boolean swapped = false;
			for(int j = 0;j < n - i - 1;j++) {
				if(list.get(j).getIndex(width) > list.get(j + 1).getIndex(width)) {
					swap(j, j + 1);
					swapped = true;
				}
			}
			if(!swapped)
				break;
		}
		sortLock = false;
	}
	
	private void swap(int i, int j) {
		GameObject swap = list.get(i);
		list.set(i, list.get(j));
		list.set(j, swap);
	}
	
}
