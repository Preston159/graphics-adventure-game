package com.preston159.gag.world;

public class Location {
	
	private Tuple<Double> loc;
	
	public Location(double x, double y) {
		loc = new Tuple<Double>(x, y);
	}
	
	public Location(Tuple<? extends Number> loc) {
		this.loc = new Tuple<Double>((double) loc.x, (double) loc.y);
	}
	
	public double getX() {
		return loc.x;
	}
	
	public double getY() {
		return loc.y;
	}
	
	public int getBlockX() {
		return (int) (this.loc.x + 0.5);
	}
	
	public int getBlockY() {
		return (int) (this.loc.y + 0.5);
	}
	
	public void setX(double x) {
		this.loc.x = x;
	}
	
	public void setY(double y) {
		this.loc.y = y;
	}
	
	public void changeX(double dx) {
		this.loc.x += dx;
	}
	
	public void changeY(double dy) {
		this.loc.y += dy;
	}
	
	public Location copy() {
		return new Location(this.loc.copy());
	}
	
}
