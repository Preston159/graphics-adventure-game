package com.preston159.gag.main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.Timer;

import com.preston159.gag.character.Player;
import com.preston159.gag.display.Settings;
import com.preston159.gag.display.Direction;
import com.preston159.gag.display.GameObject;
import com.preston159.gag.display.Keyboard;
import com.preston159.gag.display.Sprite;
import com.preston159.gag.display.Window;
import com.preston159.gag.display.gui.HUD;
import com.preston159.gag.world.Location;
import com.preston159.gag.world.World;

public class Main {
	
	public enum SUCCESS { TRUE, FALSE, PARTIAL }
	
	private static Timer drawTimer;
	
	private static Player player;
	private static HUD hud;
	
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setSize(Settings.WINDOW_SIZE);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("");
		
		Window window = new Window(Settings.WINDOW_SIZE);
		frame.setContentPane(window);
		frame.addKeyListener(new Keyboard());
		
		World world = new World(512, 512, -1, -1);
		
		BufferedImage sheet = null;
		try {
			sheet = ImageIO.read(new File("img/sheet1.png"));
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
		Sprite playerSprite = new Sprite(sheet, 5 * 16, 9 * 16, 16, 16, Settings.SQUARE_SIZE, Settings.SQUARE_SIZE);
		GameObject playerObj = new GameObject(playerSprite, new Location(7, 7), true);
		player = new Player(playerObj);
		hud = new HUD(player);
		
		Sprite testSprite = new Sprite(sheet, 0, 7 * 16, 16, 16, Settings.SQUARE_SIZE, Settings.SQUARE_SIZE);
		GameObject testObject = new GameObject(testSprite, new Location(0, 0), false);
		
		GameObject testObject2 = testObject.copy();
		testObject2.getLocation().changeX(1);
		
		GameObject testObject3 = testObject.copy();
		testObject3.getLocation().changeY(1);
		
		GameObject testObject4 = testObject.copy();
		testObject4.getLocation().changeX(1);
		testObject4.getLocation().changeY(1);
		
		
		world.addObject(player.getObject());
		world.addObject(testObject);
		world.addObject(testObject2);
		world.addObject(testObject3);
		world.addObject(testObject4);
		
		drawTimer = new Timer(40, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				world.sortList();
				//perform movements
				MOVEX: {
					if(Keyboard.keyPressed(Direction.RIGHT)) {
						if(player.getObject().colliding(Direction.RIGHT))
							break MOVEX;
						world.changeXOff(Settings.MOVE_SPEED);
						player.getObject().changeX(Settings.MOVE_SPEED);
					} else if(Keyboard.keyPressed(Direction.LEFT)) {
						if(player.getObject().colliding(Direction.DOWN))
							break MOVEX;
						world.changeXOff(-1 * Settings.MOVE_SPEED);
						player.getObject().changeX(-1 * Settings.MOVE_SPEED);
					}
				}
				MOVEY: {
					if(Keyboard.keyPressed(Direction.DOWN)) {
						if(player.getObject().colliding(Direction.DOWN))
							break MOVEY;
						world.changeYOff(Settings.MOVE_SPEED);
						player.getObject().changeY(Settings.MOVE_SPEED);
					} else if(Keyboard.keyPressed(Direction.UP)) {
						if(player.getObject().colliding(Direction.UP))
							break MOVEY;
						world.changeYOff(-1 * Settings.MOVE_SPEED);
						player.getObject().changeY(-1 * Settings.MOVE_SPEED);
					}
				}
				//
				window.draw(world, hud);
			}
		});
		drawTimer.start();
	}
	
}
