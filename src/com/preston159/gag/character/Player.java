package com.preston159.gag.character;

import com.preston159.gag.display.Direction;
import com.preston159.gag.display.GameObject;
import com.preston159.gag.display.Settings;
import com.preston159.gag.world.World;

public class Player extends Character {
	
	private GameObject object;
	
	public Player(GameObject object) {
		this.object = object;
	}

	public GameObject getObject() {
		return object;
	}
	
	public void move(Direction direction, World world) {
		if(this.getObject().colliding(direction))
			return;
		double amt = Settings.MOVE_SPEED;
	}
	
}
