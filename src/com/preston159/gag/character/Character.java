package com.preston159.gag.character;

import com.preston159.gag.display.GameObject;

public abstract class Character {
	
	public abstract GameObject getObject();
	
}
