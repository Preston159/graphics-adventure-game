package com.preston159.gag.display;

import java.awt.Image;
import java.awt.image.BufferedImage;

public class Sprite {
	
	private Image image;
	
	/**
	 * Creates a sprite
	 * @param sheet	the sprite sheet
	 * @param x		the x-location of the top-left corner of the sprite on the sheet
	 * @param y		the y-location of the top-left corner of the sprite on the sheet
	 * @param iw	the width of the sprite on the sheet
	 * @param ih	the height of the sprite on the sheet
	 * @param w		the width of the output sprite
	 * @param h		the height of the output sprite
	 */
	public Sprite(BufferedImage sheet, int x, int y, int iw, int ih, int w, int h) {
		this.image = sheet.getSubimage(x, y, iw, ih).getScaledInstance(w, h, 0);
	}
	
	/**
	 * Creates a sprite
	 * @param sheet	the sprite sheet
	 * @param x		the x-location of the top-left corner of the sprite on the sheet
	 * @param y		the y-location of the top-left corner of the sprite on the sheet
	 * @param w		the width of the sprite
	 * @param h		the height of the sprite
	 */
	public Sprite(BufferedImage sheet, int x, int y, int w, int h) {
		this(sheet, x, y, w, h, w, h);
	}
	
	/**
	 * Creates a sprite
	 * @param sprite	the sprite image
	 * @param w			the width of the output sprite
	 * @param h			the height of the output sprite
	 */
	public Sprite(BufferedImage sprite, int w, int h) {
		this(sprite, 0, 0, sprite.getWidth(), sprite.getHeight(), w, h);
	}
	
	/**
	 * Creates a sprite
	 * @param sprite	the sprite image
	 */
	public Sprite(BufferedImage sprite) {
		this(sprite, 0, 0, sprite.getWidth(), sprite.getHeight());
	}
	
	public Image getImage() {
		return image;
	}
	
}
