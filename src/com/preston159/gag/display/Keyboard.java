package com.preston159.gag.display;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {
	
	private static boolean LEFT = false;
	private static boolean RIGHT = false;
	private static boolean UP = false;
	private static boolean DOWN = false;

	@Override
	public void keyPressed(final KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			LEFT = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			RIGHT = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			UP = true;
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			DOWN = true;
		}
	}

	@Override
	public void keyReleased(final KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			LEFT = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			RIGHT = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			UP = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			DOWN = false;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
	
	public static boolean keyPressed(Direction direction) {
		switch(direction) {
		case UP:
			return UP;
		case DOWN:
			return DOWN;
		case LEFT:
			return LEFT;
		case RIGHT:
			return RIGHT;
		}
		return false;
	}

}
