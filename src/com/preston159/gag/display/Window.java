package com.preston159.gag.display;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.preston159.gag.display.gui.GUI;
import com.preston159.gag.world.World;

@SuppressWarnings("serial")
public class Window extends JPanel {
	
	public Window(Dimension size) {
		super();
		this.setSize(size);
	}
	
	public void draw(World world, GUI gui) {
		this.removeAll();
		BufferedImage display = new BufferedImage(super.getSize().width, super.getSize().height, BufferedImage.TYPE_3BYTE_BGR);
		//draw hud
		ArrayList<GameObject> visible = world.getVisibleObjects(this);
		//draw objects
		for(GameObject object : visible) {
			if(!object.isForeground())
				object.draw(display, world.getOffset());
		}
		for(GameObject object : visible) {
			if(object.isForeground())
				object.draw(display, world.getOffset());
		}
		this.getGraphics().drawImage(display, 0, 0, null);
		gui.draw(display);
	}
	
}
