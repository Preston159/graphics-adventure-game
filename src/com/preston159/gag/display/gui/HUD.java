package com.preston159.gag.display.gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;

import com.preston159.gag.character.Player;

public class HUD extends GUI {
	
	private Player player;
	
	public HUD(Player player) {
		this.player = player;
	}
	
	public void draw(BufferedImage display) {
		Graphics2D graphics = display.createGraphics();
		
		graphics.setColor(Color.WHITE);
		graphics.fill(new Ellipse2D.Float(0, 0, 200, 100));
		graphics.dispose();
	}
	
}
