package com.preston159.gag.display.gui;

import java.awt.image.BufferedImage;

public abstract class GUI {
	
	public abstract void draw(BufferedImage display);
	
}
