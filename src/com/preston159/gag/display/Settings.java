package com.preston159.gag.display;

import java.awt.Dimension;

public class Settings {
	
	public static final int SQUARE_SIZE = 32;
	public static final Dimension WINDOW_SIZE = new Dimension(512, 512);
	public static final double MOVE_SPEED = 0.1;
	
}
