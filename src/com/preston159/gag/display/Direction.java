package com.preston159.gag.display;

public enum Direction {
	RIGHT,
	LEFT,
	DOWN,
	UP
}
