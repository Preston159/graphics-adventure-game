package com.preston159.gag.display;

import java.awt.image.BufferedImage;

import com.preston159.gag.main.Util;
import com.preston159.gag.world.Location;
import com.preston159.gag.world.Tuple;

public class GameObject {
	
	private Location location;
	private int w, h;
	private double cw, ch;
	private Sprite sprite;
	private boolean foreground;
	
	public GameObject(Sprite sprite, Location location, int w, int h, double cw, double ch, boolean foreground) {
		this.sprite = sprite;
		this.location = location;
		this.w = w;
		this.h = h;
		this.cw = cw;
		this.ch = ch;
		this.foreground = foreground;
	}
	
	public GameObject(Sprite sprite, Location location, int w, int h, boolean foreground) {
		this(sprite, location, w, h, (double) w / Settings.SQUARE_SIZE, (double) h / Settings.SQUARE_SIZE, foreground);
	}
	
	public GameObject(Sprite sprite, Location location, boolean foreground) {
		this(sprite, location, sprite.getImage().getWidth(null), sprite.getImage().getHeight(null), foreground);
	}
	
	public Location getLocation() {
		return location;
	}
	
	public int getIndex(int width) {
		return (location.getBlockY() * width) + location.getBlockX();
	}
	
	public int getWidth() {
		return w;
	}
	
	public int getHeight() {
		return h;
	}
	
	public double getCollisionWidth() {
		return cw;
	}
	
	public double getCollisionHeight() {
		return ch;
	}
	
	public Location getCollisionCorner(Corner corner) {
		Location up = getCollisionSide(Direction.UP);
		Location down = getCollisionSide(Direction.DOWN);
		Location left = getCollisionSide(Direction.LEFT);
		Location right = getCollisionSide(Direction.RIGHT);
		switch(corner) {
		case TOP_LEFT:
			return new Location(left.getX(), up.getY());
		case TOP_RIGHT:
			return new Location(right.getX(), up.getY());
		case BOTTOM_LEFT:
			return new Location(left.getX(), down.getY());
		case BOTTOM_RIGHT:
			return new Location(right.getX(), down.getY());
		}
		return null;
	}
	
	public Location getCollisionSide(Direction direction) {
		Location out = this.getCenter();
		switch(direction) {
		case RIGHT:
			out.changeX(this.getCollisionWidth() / 2);
			break;
		case LEFT:
			out.changeX(-1 * (this.getCollisionWidth() / 2));
			break;
		case DOWN:
			out.changeY(this.getCollisionWidth() / 2);
			break;
		case UP:
			out.changeY(-1 * (this.getCollisionWidth() / 2));
			break;
		}
		return out;
	}
	
	public Location getCenter() {
		Location center = this.location.copy();
		center.changeX((double) this.w / Settings.SQUARE_SIZE / 2);
		center.changeY((double) this.h / Settings.SQUARE_SIZE / 2);
		return center;
	}
	
	public boolean isVisible(Window window, Tuple<Double> offset) {
		Tuple<Double> position = new Tuple<Double>(location.getX() - offset.x, location.getY() - offset.y);
		Tuple<Double> tl = position.copy();
		Tuple<Double> br = position.copy();
		br.x += w;
		br.y += h;
		return
				br.x > 0 &&
				br.y > 0 &&
				tl.x < window.getWidth() &&
				tl.y < window.getHeight();
	}
	
	public void draw(BufferedImage display, Tuple<Double> offset) {
		display.getGraphics().drawImage(
				this.sprite.getImage(),
				Util.locToPix(this.location.getX() - offset.x),
				Util.locToPix(this.location.getY() - offset.y),
				null
				);
	}
	
	public GameObject copy() {
		return new GameObject(this.sprite, this.location.copy(), this.w, this.h, this.foreground);
	}
	
	public void changeX(double dx) {
		this.location.changeX(dx);
	}
	
	public void changeY(double dy) {
		this.location.changeY(dy);
	}
	
	public boolean isForeground() {
		return this.foreground;
	}
	
	public boolean colliding(Direction direction) {
		return direction == Direction.UP;
	}
	
}
